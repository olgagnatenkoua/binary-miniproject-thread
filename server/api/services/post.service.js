import postRepository from '../../data/repositories/post.repository';
import postReactionRepository from '../../data/repositories/post-reaction.repository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
    ...post,
    userId
});

export const update = async (post) => {
    const { imageId, body, userId } = post;
    await postRepository.updateById(post.id, {
        userId,
        body,
        imageId
    });
    const response = await postRepository.getPostById(post.id);
    return response;
};

export const setReaction = async (userId, { postId, isLike = true }) => {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
        ? postReactionRepository.deleteById(react.id)
        : postReactionRepository.updateById(react.id, { isLike }));

    const reaction = await postReactionRepository.getPostReaction(
        userId,
        postId
    );

    const result = reaction
        ? await updateOrDelete(reaction)
        : await postReactionRepository.create({ userId, postId, isLike });

    // the result is an integer when an entity is deleted
    const entityDeleted = Number.isInteger(result);

    const isNewReaction = !reaction;
    let response = {
        isNewReaction
    };
    if (!entityDeleted) {
        response = await postReactionRepository.getPostReaction(userId, postId);
        response.dataValues.isNewReaction = isNewReaction;
    }

    return response;
};

/* eslint-disable no-case-declarations */
import {
    SET_ALL_POSTS,
    LOAD_MORE_POSTS,
    ADD_POST,
    SET_EXPANDED_POST,
    UPDATE_POST
} from './actionTypes';

export default (state = {}, action) => {
    switch (action.type) {
        case SET_ALL_POSTS:
            return {
                ...state,
                posts: action.posts,
                hasMorePosts: Boolean(action.posts.length)
            };
        case LOAD_MORE_POSTS:
            return {
                ...state,
                posts: [...(state.posts || []), ...action.posts],
                hasMorePosts: Boolean(action.posts.length)
            };
        case ADD_POST:
            return {
                ...state,
                posts: [action.post, ...state.posts]
            };
        case UPDATE_POST:
            const { posts } = state;
            const { post } = action;
            let postIndex = -1;
            if (post) {
                postIndex = posts.findIndex(item => item.id === post.id);
            }
            const updatedPosts = postIndex === -1
                ? posts
                : Object.assign([], posts, {
                    [postIndex]: post
                });
            return {
                ...state,
                posts: updatedPosts
            };
        case SET_EXPANDED_POST:
            return {
                ...state,
                expandedPost: action.post
            };
        default:
            return state;
    }
};

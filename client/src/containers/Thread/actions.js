import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
    ADD_POST,
    UPDATE_POST,
    LOAD_MORE_POSTS,
    SET_ALL_POSTS,
    SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
    type: SET_ALL_POSTS,
    posts
});

const addMorePostsAction = posts => ({
    type: LOAD_MORE_POSTS,
    posts
});

const addPostAction = post => ({
    type: ADD_POST,
    post
});

const updatePostAction = post => ({
    type: UPDATE_POST,
    post
});

const setExpandedPostAction = post => ({
    type: SET_EXPANDED_POST,
    post
});

const setPostsAllowChange = (posts, userId) => posts.map(post => ({
    ...post,
    allowChange: userId === post.user.id
}));

export const loadPosts = filter => async (dispatch, getRootState) => {
    const {
        profile: { user }
    } = getRootState();

    const posts = await postService.getAllPosts(filter);
    const enrichedPosts = setPostsAllowChange(posts, user.id);
    dispatch(setPostsAction(enrichedPosts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
    const {
        profile: { user },
        posts: { posts }
    } = getRootState();
    const loadedPosts = await postService.getAllPosts(filter);
    const filteredPosts = loadedPosts.filter(
        post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
    );
    const enrichedPosts = setPostsAllowChange(filteredPosts, user.id);
    dispatch(addMorePostsAction(enrichedPosts));
};

export const applyPost = postId => async (dispatch) => {
    const post = await postService.getPost(postId);
    dispatch(addPostAction(post));
};

export const addPost = post => async (dispatch) => {
    const { id } = await postService.addPost(post);
    const newPost = await postService.getPost(id);
    newPost.allowChange = true; // post was added by the current user, so he owns it
    dispatch(addPostAction(newPost));
};

export const updatePost = post => async (dispatch) => {
    const { id, userId, body, imageId } = post;
    const postForUpdate = {
        id,
        userId,
        body,
        imageId
    };
    const updatedPost = await postService.updatePost(postForUpdate);
    updatedPost.allowChange = true;
    dispatch(updatePostAction(updatedPost));
};

export const toggleExpandedPost = postId => async (dispatch) => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setExpandedPostAction(post));
};

const updateReactions = (post, likeDiff, dislikeDiff) => ({
    ...post,
    likeCount: Number(post.likeCount) + likeDiff,
    dislikeCount: Number(post.dislikeCount) + dislikeDiff
});

export const likePost = postId => async (dispatch, getRootState) => {
    const { id, isNewReaction } = await postService.likePost(postId);
    const likeDiff = id || isNewReaction ? 1 : -1; // if ID exists then the post was liked (record was created)
    const dislikeDiff = !id || isNewReaction ? 0 : -1;

    const {
        posts: { posts, expandedPost }
    } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : updateReactions(post, likeDiff, dislikeDiff)));

    dispatch(setPostsAction(updated));
    if (expandedPost && expandedPost.id === postId) {
        dispatch(
            setExpandedPostAction(
                updateReactions(expandedPost, likeDiff, dislikeDiff)
            )
        );
    }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
    const { id, isNewReaction } = await postService.dislikePost(postId);
    const dislikeDiff = id || isNewReaction ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed
    const likeDiff = !id || isNewReaction ? 0 : -1;

    const {
        posts: { posts, expandedPost }
    } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : updateReactions(post, likeDiff, dislikeDiff)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(
            setExpandedPostAction(
                updateReactions(expandedPost, likeDiff, dislikeDiff)
            )
        );
    }
};

export const addComment = request => async (dispatch, getRootState) => {
    const { id } = await commentService.addComment(request);
    const comment = await commentService.getComment(id);

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) + 1,
        comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const {
        posts: { posts, expandedPost }
    } = getRootState();
    const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));
    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

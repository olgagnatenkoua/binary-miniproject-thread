import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import EditedPost from 'src/containers/EditedPost';
import Post from 'src/components/Post';
import AddUpdatePost from 'src/components/AddUpdatePost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
    loadPosts,
    loadMorePosts,
    likePost,
    dislikePost,
    toggleExpandedPost,
    updatePost,
    addPost
} from './actions';

import styles from './styles.module.scss';

class Thread extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sharedPostId: undefined,
            showOwnPosts: false,
            editedPost: undefined
        };
        this.postsFilter = {
            userId: this.props.userId,
            from: 0,
            count: 10
        };
    }

    tooglePosts = () => {
        this.setState(
            ({ showOwnPosts }) => ({ showOwnPosts: !showOwnPosts }),
            () => {
                Object.assign(this.postsFilter, {
                    userId: this.props.userId,
                    from: 0,
                    showOwnPosts: this.state.showOwnPosts
                });
                this.props.loadPosts(this.postsFilter);
                this.postsFilter.from = this.postsFilter.count; // for next scroll
            }
        );
    };

    loadMorePosts = () => {
        this.props.loadMorePosts(this.postsFilter);
        const { from, count } = this.postsFilter;
        this.postsFilter.from = from + count;
    };

    sharePost = (sharedPostId) => {
        this.setState({ sharedPostId });
    };

    updatePost = async (post) => {
        await this.props.updatePost(post);
        this.closeEditedPost();
    };

    openEditedPost = (postId) => {
        const { posts } = this.props;
        const editedPost = posts.find(post => post.id === postId);
        this.setState({ editedPost });
    };

    closeEditedPost = () => {
        this.setState({ editedPost: undefined });
    };

    closeSharePost = () => {
        this.setState({ sharedPostId: undefined });
    };

    uploadImage = file => imageService.uploadImage(file);

    render() {
        const { posts = [], expandedPost, hasMorePosts, ...props } = this.props;
        const { showOwnPosts, sharedPostId, editedPost } = this.state;
        return (
            <div className={styles.threadContent}>
                <div className={styles.addPostForm}>
                    <AddUpdatePost
                        editMode={false}
                        addPost={props.addPost}
                        uploadImage={this.uploadImage}
                    />
                </div>
                <div className={styles.toolbar}>
                    <Checkbox
                        toggle
                        label="Show only my posts"
                        checked={showOwnPosts}
                        onChange={this.tooglePosts}
                    />
                </div>
                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.loadMorePosts}
                    hasMore={hasMorePosts}
                    loader={<Loader active inline="centered" key={0} />}
                >
                    {posts.map(post => (
                        <Post
                            post={post}
                            likePost={props.likePost}
                            dislikePost={props.dislikePost}
                            toggleExpandedPost={props.toggleExpandedPost}
                            openEditedPost={this.openEditedPost}
                            sharePost={this.sharePost}
                            key={post.id}
                        />
                    ))}
                </InfiniteScroll>
                {expandedPost && <ExpandedPost sharePost={this.sharePost} />}
                {editedPost && (
                    <EditedPost
                        post={editedPost}
                        updatePost={this.updatePost}
                        closeEditedPost={this.closeEditedPost}
                    />
                )}
                {sharedPostId && (
                    <SharedPostLink
                        postId={sharedPostId}
                        close={this.closeSharePost}
                    />
                )}
            </div>
        );
    }
}

Thread.propTypes = {
    posts: PropTypes.arrayOf(PropTypes.object),
    hasMorePosts: PropTypes.bool,
    expandedPost: PropTypes.objectOf(PropTypes.any),
    editedPost: PropTypes.objectOf(PropTypes.any),
    sharedPostId: PropTypes.string,
    userId: PropTypes.string,
    loadPosts: PropTypes.func.isRequired,
    loadMorePosts: PropTypes.func.isRequired,
    likePost: PropTypes.func.isRequired,
    dislikePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    addPost: PropTypes.func.isRequired,
    updatePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
    posts: [],
    hasMorePosts: true,
    expandedPost: undefined,
    sharedPostId: undefined,
    userId: undefined,
    editedPost: undefined
};

const mapStateToProps = rootState => ({
    posts: rootState.posts.posts,
    hasMorePosts: rootState.posts.hasMorePosts,
    expandedPost: rootState.posts.expandedPost,
    userId: rootState.profile.user.id
});

const actions = {
    loadPosts,
    loadMorePosts,
    likePost,
    dislikePost,
    toggleExpandedPost,
    addPost,
    updatePost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Thread);

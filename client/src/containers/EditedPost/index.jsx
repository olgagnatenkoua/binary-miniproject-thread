import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal } from 'semantic-ui-react';
import * as imageService from 'src/services/imageService';
import { updatePost } from 'src/containers/Thread/actions';
import AddUpdatePost from 'src/components/AddUpdatePost';
import Spinner from 'src/components/Spinner';

class EditedPost extends React.Component {
    state = {
        open: true
    };

    closeModal = () => {
        this.props.closeEditedPost();
    };

    handleUpdatePost = async (post) => {
        await this.props.updatePost(post);
        this.closeModal();
    };

    uploadImage = file => imageService.uploadImage(file);

    render() {
        const { post } = this.props;
        return (
            <Modal
                dimmer="blurring"
                centered
                open={this.state.open}
                onClose={this.closeModal}
            >
                {post ? (
                    <Modal.Content>
                        <AddUpdatePost
                            editMode
                            post={post}
                            updatePost={this.handleUpdatePost}
                            uploadImage={this.uploadImage}
                        />
                    </Modal.Content>
                ) : (
                    <Spinner />
                )}
            </Modal>
        );
    }
}

EditedPost.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    closeEditedPost: PropTypes.func.isRequired,
    updatePost: PropTypes.func.isRequired
};

const actions = { updatePost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    null,
    mapDispatchToProps
)(EditedPost);

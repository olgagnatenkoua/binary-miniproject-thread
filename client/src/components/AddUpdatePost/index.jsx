import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Segment } from 'semantic-ui-react';

import styles from './styles.module.scss';

const initialState = {
    body: '',
    imageId: undefined,
    imageLink: undefined
};

class AddUpdatePost extends React.Component {
    static defaultProps = {
        editMode: false,
        post: undefined,
        addPost: null,
        updatePost: null
    };

    constructor(props) {
        super(props);
        this.state = {
            ...initialState,
            isUploading: false
        };

        const { editMode, post } = props;
        if (editMode) {
            this.state.body = post.body;
            const { image } = post;
            this.state.imageId = image && image.id;
            this.state.imageLink = image && image.link;
        }
    }

    handleUpdatePost = async () => {
        const { post } = this.props;
        const { imageId, body } = this.state;
        const updatedPost = {
            ...post,
            imageId,
            body
        };
        await this.props.updatePost(updatedPost);
    };

    handleAddUpdatePost = async () => {
        const { editMode, updatePost, addPost } = this.props;
        if (editMode && updatePost) {
            await this.handleUpdatePost();
        } else if (addPost) {
            await this.handleAddPost();
        }
        this.setState(initialState);
    };

    handleAddPost = async () => {
        const { imageId, body } = this.state;
        if (!body) {
            return;
        }
        await this.props.addPost({ imageId, body });
    };

    handleUploadFile = async ({ target }) => {
        this.setState({ isUploading: true });
        try {
            const {
                id: imageId,
                link: imageLink
            } = await this.props.uploadImage(target.files[0]);
            this.setState({ imageId, imageLink, isUploading: false });
        } catch {
            // TODO: show error
            this.setState({ isUploading: false });
        }
    };

    render() {
        const { editMode } = this.props;
        const { imageLink, body, isUploading } = this.state;
        let imgBtnText = 'Attach Image';
        if (editMode && imageLink) {
            imgBtnText = 'Update Image';
        }
        return (
            <Segment>
                <Form onSubmit={this.handleAddUpdatePost}>
                    <Form.TextArea
                        name="body"
                        value={body}
                        placeholder="What is the news?"
                        onChange={ev => this.setState({ body: ev.target.value })
                        }
                    />
                    {imageLink && (
                        <div className={styles.imageWrapper}>
                            <Image
                                className={styles.image}
                                src={imageLink}
                                alt="post"
                            />
                        </div>
                    )}
                    <Button
                        color="teal"
                        icon
                        labelPosition="left"
                        as="label"
                        loading={isUploading}
                        disabled={isUploading}
                    >
                        <Icon name="image" />
                        {imgBtnText}
                        <input
                            name="image"
                            type="file"
                            onChange={this.handleUploadFile}
                            hidden
                        />
                    </Button>
                    <Button
                        floated="right"
                        color="blue"
                        type="submit"
                        disabled={isUploading}
                    >
                        {editMode ? 'Update Post' : 'Post'}
                    </Button>
                </Form>
            </Segment>
        );
    }
}

AddUpdatePost.propTypes = {
    addPost: PropTypes.func,
    updatePost: PropTypes.func,
    uploadImage: PropTypes.func.isRequired,
    editMode: PropTypes.bool,
    post: PropTypes.objectOf(PropTypes.any)
};

export default AddUpdatePost;
